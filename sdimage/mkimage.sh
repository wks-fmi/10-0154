#!/bin/bash

set -e

files="../bootloader/software/spl_bsp/uboot-socfpga/u-boot.img \
       ../bootloader/software/spl_bsp/uboot-socfpga/spl/preloader.img \
       ../devtree/soc_system.dtb"

for file in $files
do
    if [ ! -f "$file" ]; then
        echo "$file not found!"
        exit 1
    fi
    cp $file .
done

mv soc_system.dtb socfpga.dtb

mkdir -p rootfs
tar -xzvf rootfs.tar.gz -C rootfs

sudo ./make_sdimage.py -f -P preloader.img,u-boot.img,num=3,format=raw,size=10M,type=A2 -P rootfs/*,num=2,format=ext3,size=900M -P zImage,u-boot.scr,soc_system.rbf,socfpga.dtb,num=1,format=fat32,size=90M -s 1G -n sdcard.img
